FROM selenium/standalone-chrome

USER root

RUN apt-get update \
    && apt-get install -y python3 python3-pip \
    && ln -sf /usr/bin/python3 /usr/bin/python \
    && ln -sf /usr/bin/pip3 /usr/bin/pip

COPY . /app/
USER seluser